describe('E2E: testing globalMessageDirective', function() {

    var basePath = 'http://localhost:3000/';

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should global message exists', function () {
        expect(element(by.className('b-global-message')).isPresent()).toBe(true);
    });

});