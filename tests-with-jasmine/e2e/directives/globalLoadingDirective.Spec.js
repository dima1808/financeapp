describe('E2E: testing globalLoadingDirective', function() {

    var basePath = 'http://localhost:3000/';

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should global loading exists', function () {
        expect(element(by.className('b-global-loading')).isPresent()).toBe(true);
    });

});