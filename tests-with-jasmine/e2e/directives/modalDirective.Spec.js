describe('E2E: testing modalDirective', function() {

    var basePath = 'http://localhost:3000/';

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should modal window exists', function () {
        expect(element(by.className('finance-modal')).isPresent()).toBe(true);
    });

});