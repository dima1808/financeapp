describe('E2E: testing financeItemDirective', function() {

    var basePath = 'http://localhost:3000/';

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should finance item exists', function () {
        expect(element(by.className('b-finance-item')).isPresent()).toBe(true);
    });

});