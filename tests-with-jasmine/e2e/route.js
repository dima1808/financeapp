describe('E2E: testing routes', function() {

    var basePath = 'http://localhost:3000/';

    it('should load the yahoo page', function() {
        browser.get(basePath);
        expect(browser.getCurrentUrl())
            .toBe(basePath + "#/yahoo");
    });

    it('should load the google index', function() {
        browser.setLocation('google');
        expect(browser.getCurrentUrl())
            .toBe(basePath + "#/google");
    });

    it('should go to google page', function() {
        browser.get(basePath);
        var mainLink = element(by.css('.main-nav a[href$=google]'));
        mainLink.click();
        expect(browser.getCurrentUrl())
            .toBe(basePath + "#/google");
    });

    it('should show error message', function() {
        browser.get(basePath + '#/bla');
        var alertDanger = element(by.css('.b-alert.b-alert_danger'));
        expect(alertDanger.$('.b-alert__message').getText())
            .toBe('You have set wrong api name');
        alertDanger.getAttribute('class').then(function(classValue) {
            expect(classValue.indexOf('ng-hide') === -1).toBeTruthy();
        });
    });

});