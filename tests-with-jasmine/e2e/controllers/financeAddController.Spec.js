describe('E2E: testing FinanceAddController', function() {

    var basePath = 'http://localhost:3000/';
    var input = element(by.model('financeAddCtrl.company.name'));
    var form = element(by.name('financeAddForm'));

    beforeEach(function() {
        browser.get(basePath);
    });

    it('should add info', function() {
        var baseCount = element.all(by.css('.b-finance-item__head h3')).count();

        baseCount.then(function(value) {

            input.sendKeys('III');
            var promise = form.submit();

            promise.then(function() {
                element.all(by.css('.b-finance-item__head h3'))
                    .then(function(items) {
                        expect(items.length).toBe(value + 1);
                        expect(items[value].getText()).toBe('III');
                    });
            });

        });
    });

    it('should not add info', function() {
        input.sendKeys('---');
        form.submit();
        var errorMessageBlock = element(by.className('one-form-field-error-message'));
        expect(errorMessageBlock.getCssValue('display')).toBe('block');
    });

    it('should show error message', function() {

        input.sendKeys('notExistCompany');

        form.submit().then(function() {
            var errorMessage = element(by.css('[global-message]'));
            errorMessage.getAttribute('class')
                .then(function(classValue) {
                    expect(classValue.indexOf('b-global-message_shown_error') !== -1).toBeTruthy();
                });
        });

    });

});