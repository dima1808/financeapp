describe('E2E: testing FinanceDataController', function() {

    var basePath = 'http://localhost:3000';
    var modal = element(by.css('.b-finance-info [modal]'));
    var financeItemBtn = element(by.css('.b-finance-item__remove button'));

    beforeEach(function() {
        browser.get(basePath);
    });

    it('should show information about company', function() {
        var financeItems = element.all(by.css('.b-finance-item__head h3'))
            .then(function(items) {
                expect(items[0].getText()).toBe('EPAM');
            });
    });

    it('should show modal window', function() {
        modal.getAttribute('class').then(function(classValue) {
            expect(classValue.indexOf('ng-hide') !== -1).toBeTruthy();
            financeItemBtn.click();
            modal.getAttribute('class')
                .then(function(classValue) {
                    expect(classValue.indexOf('ng-hide') === -1).toBeTruthy();
                });
        });
    });

    it('should show and close modal window', function() {
        var modalCancelBtn = element(by.css('.b-finance-info [modal] .b-btn.b-btn_danger'));
        financeItemBtn.click();
        modalCancelBtn.click();
        modal.getAttribute('class')
            .then(function(classValue) {
                expect(classValue.indexOf('ng-hide') !== -1).toBeTruthy();
            });
    });

    it('should delete information about company', function() {
        var modalConfirmBtn = element(by.css('.b-finance-info [modal] .b-btn.b-btn_primary'));
        financeItemBtn.click();
        modalConfirmBtn.click();
        var financeItem = element(by.css('.b-finance-item__head h3'));
        expect(financeItem.getText()).not.toBe('EPAM');
    });

});