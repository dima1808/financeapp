describe('check work of googleFinanceService', function() {
    beforeEach(module('myApp'));

    var baseCompanyName = 'EPAM';
    var fakeCompanyName = 'abfdhfhrre';
    var baseGroupCompanyNames = ['EPAM', 'IBM'];
    var baseQuery = 'http://www.google.com/finance/info?callback=JSON_CALLBACK&q=';
    var $httpBackend;
    var googleFinanceService;

    beforeEach(inject(function(_$injector_) {
        googleFinanceService = _$injector_.get('googleFinanceService');
        $httpBackend = _$injector_.get('$httpBackend');
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('Should return information about ' + baseCompanyName, function() {

        $httpBackend.expect('JSONP', baseQuery + baseCompanyName)
            .respond(200, [{
                t: baseCompanyName,
                c: '10',
                cp: 10,
                l: 100
            }]);

        googleFinanceService.getAll(baseCompanyName).then(function(data) {
            expect(data[0].symbol).toBe(baseCompanyName);
        });

        $httpBackend.flush();
    });

    it('Should return information about group of companies ' + baseGroupCompanyNames.join(', '), function() {

        $httpBackend.expect('JSONP', baseQuery + baseGroupCompanyNames.join(','))
            .respond(200, [{
                t: baseGroupCompanyNames[0],
                c: '10',
                cp: 10,
                l: 100
            }, {
                t: baseGroupCompanyNames[1],
                c: '10',
                cp: 10,
                l: 100
            }]);

        googleFinanceService.getAll(baseGroupCompanyNames).then(function(data) {
            expect(data[0].symbol).toBe(baseGroupCompanyNames[0]);
            expect(data[1].symbol).toBe(baseGroupCompanyNames[1]);
        });

        $httpBackend.flush();
    });

    it('Should not return info about wrong company', function() {

        var wrongData = false;
        $httpBackend.expect('JSONP', baseQuery + fakeCompanyName)
            .respond(404, {});

        googleFinanceService.getAll(fakeCompanyName).catch(function(data) {
            wrongData = true;
        });

        $httpBackend.flush();
        expect(wrongData).toBeTruthy();
    });

});