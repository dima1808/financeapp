describe('check work of financeService', function() {
    beforeEach(module('myApp'));

    var baseCompanyName = 'EPAM';
    var baseQuery = 'http://www.google.com/finance/info?callback=JSON_CALLBACK&q=';
    var financeApiConst;
    var financeService;
    var $httpBackend;

    beforeEach(inject(function(_$injector_) {

        financeService = _$injector_.get('financeService');
        $httpBackend = _$injector_.get('$httpBackend');
        financeApiConst = _$injector_.get('financeApiConst');

        $httpBackend.expect('JSONP', baseQuery + baseCompanyName)
            .respond(200, [{
                t: baseCompanyName,
                c: '10',
                cp: 10,
                l: 100
            }]);

    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should return name of one company', function() {

        financeService.getAll(baseCompanyName, true, financeApiConst.google);

        $httpBackend.flush();

        var companyNames = financeService.getCurrentDataNames();
        expect(companyNames[0]).toEqual(baseCompanyName);

    });

    it('should remove company from the list', function() {

        financeService.getAll(baseCompanyName, true, financeApiConst.google);

        $httpBackend.flush();

        var companyNames = financeService.getCurrentDataNames();
        expect(companyNames.length).toEqual(1);

        financeService.remove(baseCompanyName);

        companyNames = financeService.getCurrentDataNames();
        expect(companyNames.length).toEqual(0);

    });

});