describe('check work of yahooFinanceService', function() {
    beforeEach(module('myApp'));

    var baseCompanyName = 'EPAM';
    var baseGroupCompanyNames = ['EPAM', 'IBM'];
    var info = [
        'symbol',
        'LastTradePriceOnly',
        'Change_PercentChange',
    ];
    var baseQuery = 'https://query.yahooapis.com/v1/public/yql?env=' +
        encodeUriQuery('store://datatables.org/alltableswithkeys') +
        '&format=json&q=';
    var $httpBackend;
    var yahooFinanceService;

    beforeEach(inject(function(_$injector_) {
        yahooFinanceService = _$injector_.get('yahooFinanceService');
        $httpBackend = _$injector_.get('$httpBackend');
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('Should return information about ' + baseCompanyName, function() {

        var q = encodeUriQuery('select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (\'' + baseCompanyName + '\')');
        var baseChange = 10;
        $httpBackend.expect('GET', baseQuery + q)
            .respond(200, [{
                symbol: baseCompanyName,
                Change_PercentChange: baseChange + ' - 10%',
                LastTradePriceOnly: 100
            }]);

        yahooFinanceService.getAll(baseCompanyName).then(function(data) {
            expect(data[0].symbol).toBe(baseCompanyName);
            expect(data[0].change).toBe(baseChange);
        });

        $httpBackend.flush();
    });

    it('Should return information about group of companies ' + baseGroupCompanyNames.join(', '), function() {

        var baseCompanies = baseGroupCompanyNames.map(function(x) { return '\'' + x + '\'' }).join(',');
        var q = encodeUriQuery('select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (' + baseCompanies + ')');
        $httpBackend.expect('GET', baseQuery + q)
            .respond(200, [{
                symbol: baseGroupCompanyNames[0],
                Change_PercentChange: '10 - 10%',
                LastTradePriceOnly: 100,
            }, {
                symbol: baseGroupCompanyNames[1],
                Change_PercentChange: '10 - 10%',
                LastTradePriceOnly: 100,
            }]);

        yahooFinanceService.getAll(baseGroupCompanyNames).then(function(data) {
            expect(data[0].symbol).toBe(baseGroupCompanyNames[0]);
            expect(data[1].symbol).toBe(baseGroupCompanyNames[1]);
        });

        $httpBackend.flush();
    });

    /**
     * This method is intended for encoding *key* or *value* parts of query component. We need a custom
     * method because encodeURIComponent is too aggressive and encodes stuff that doesn't have to be
     * encoded per http://tools.ietf.org/html/rfc3986:
     *    query       = *( pchar / "/" / "?" )
     *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
     *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
     *    pct-encoded   = "%" HEXDIG HEXDIG
     *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
     *                     / "*" / "+" / "," / ";" / "="
     */
    function encodeUriQuery(val, pctEncodeSpaces) {
        return encodeURIComponent(val).
        replace(/%40/gi, '@').
        replace(/%3A/gi, ':').
        replace(/%24/g, '$').
        replace(/%2C/gi, ',').
        replace(/%3B/gi, ';').
        replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
    }
});