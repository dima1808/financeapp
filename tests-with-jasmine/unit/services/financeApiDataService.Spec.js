describe('check work of financeApiDataService', function() {
    beforeEach(module('myApp'));

    var financeApiDataService;
    var financeApiConst;

    beforeEach(inject(function(_$injector_) {
        financeApiDataService = _$injector_.get('financeApiDataService');
        financeApiConst = _$injector_.get('financeApiConst');
    }));

    it('should return yahoo api type', function() {
        var apiData = financeApiDataService.getApiData();
        expect(apiData.apiTypes[0].value).toBe(financeApiConst.yahoo);
    });

    it('should return that api type yahoo exists', function() {
        var isApi = financeApiDataService.isApi(financeApiConst.yahoo);
        expect(isApi).toEqual(true);
    });

    it('should return that api type bla does not exist', function() {
        var isApi = financeApiDataService.isApi('bla');
        expect(isApi).toEqual(false);
    });

});