describe('check work of baseService', function() {
    beforeEach(module('myApp'));

    var baseService;

    beforeEach(inject(function(_$injector_) {
        baseService = _$injector_.get('baseService');
    }));

    it('should return loading is true', function() {
        var loading = baseService.getGlobalLoading();
        expect(loading.show).toBeTruthy();
    });

    it('should return empty message', function() {
        var msg = baseService.getGlobalMessage();
        expect(msg.message).toBe('');
    });

    it('should return that api is correct', function() {
        baseService.setCorrectApi();
        var apiSettings = baseService.getApiSettings();
        expect(apiSettings.isCorrect).toBeTruthy();
    });

});