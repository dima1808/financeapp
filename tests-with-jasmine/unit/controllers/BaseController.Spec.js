describe('check work of BaseController', function() {
    beforeEach(module('myApp'));

    var $controller;
    var baseController;
    var baseService;

    beforeEach(inject(function(_$injector_, _$controller_) {
        $controller = _$controller_;
        baseService = _$injector_.get('baseService');
    }));

    beforeEach(function() {
        baseController = $controller('BaseController', {
           baseService: baseService,
        });
    });

    it('should globalMessage to be defined', function() {
        expect(baseController.globalMessage).toBeDefined();
    });

    it('should globalLoading to be defined', function() {
        expect(baseController.globalLoading).toBeDefined();
    });

    it('should globalCorrectApi to be defined', function() {
        expect(baseController.globalCorrectApi).toBeDefined();
    });

});