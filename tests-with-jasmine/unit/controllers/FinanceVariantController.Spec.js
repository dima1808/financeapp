describe('check work of FinanceVariantController', function() {
    beforeEach(module('myApp'));

    var $controller;
    var financeApiDataService;

    beforeEach(inject(function(_$controller_) {
        $controller = _$controller_;
        financeApiDataService = {
            getApiData: function() {
                return {
                    test: 'test',
                };
            },
        };
    }));

    it('should get correct api data', function() {
        financeVariantController = $controller('FinanceVariantController', {
            financeApiDataService: financeApiDataService,
        });
        var apiData = financeVariantController.apiItems;
        expect(apiData.test).toBe('test');
    });

});