describe('check work of FinanceDataController', function() {
    beforeEach(module('myApp'));

    var baseCompanyName = 'EPAM';
    var $controller;
    var financeApiDataService;
    var baseService;
    var financeService;
    var $scope;
    var $q;
    var $timeout;

    beforeEach(inject(function(_$timeout_, _$controller_, _$rootScope_, _$injector_) {

        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        financeApiDataService = _$injector_.get('financeApiDataService');
        baseService = _$injector_.get('baseService');
        financeService = _$injector_.get('financeService');
        $q = _$injector_.get('$q');
        $timeout = _$timeout_;

        spyOn(financeService, 'getAll').and.callFake(function() {
            return $q(function(resolve, reject){
                $timeout(function() {
                    resolve([{
                        symbol: baseCompanyName,
                    }]);
                }, 0);
            });
        });

    }));

    it('should contains information about companies', function() {
        var financeDataController = $controller('FinanceDataController', {
            $scope: $scope,
            $stateParams: {
                apiName: '',
            },
            financeApiDataService: financeApiDataService,
            baseService: baseService,
            financeService: financeService,
        });
        $timeout.flush();
        expect(financeDataController.companiesInfo.length).toBe(1);
        expect(financeDataController.companiesInfo[0].symbol).toBe(baseCompanyName);
        expect(financeDataController.companiesData.isEmpty).toBeFalsy();
    });

    it('should set error api', function() {
        var financeDataController = $controller('FinanceDataController', {
            $scope: $scope,
            $stateParams: {
                apiName: 'wrongApi',
            },
            financeApiDataService: financeApiDataService,
            baseService: baseService,
            financeService: financeService,
        });

        expect(baseService.getApiSettings().isCorrect).toBeFalsy();
    });

});