describe('check work of globalMessageDirective', function() {
    beforeEach(module('myApp'));

    var $scope;
    var testBody;
    var node;

    beforeEach(inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.modal = {
            isShown: false,
        };
        $scope.hideModal = function() {
            $scope.modal.isShown = false;
        };
        testBody = '<div class="test">Test body</div>';
        var element = angular.element('<div><div class="ng-hide" modal ' +
            'data-show="modal.isShown" ' +
            'data-close="hideModal()" data-modal-title="Test modal"> ' +
                testBody +
            '</div></div>');

        node = _$compile_(element)($scope);
        $scope.$apply();
    }));

    it('should contain h4 tag', function() {
        expect(node.html()).toContain('</h4>');
    });

    it('should not contain ng-hide class and contain it when isShown is false', function() {
        var modalBlock = node.children().eq(0);
        $scope.modal.isShown = true;
        $scope.$apply();
        expect(modalBlock.hasClass('ng-hide')).toBeFalsy();
        $scope.modal.isShown = false;
        $scope.$apply();
        expect(modalBlock.hasClass('ng-hide')).toBeTruthy();
    });

    it('should contain testBody', function() {
        expect(node.html()).toContain('Test body');
    });

    it('should contain button', function() {
        expect(node.html()).toContain('button');
    });

});