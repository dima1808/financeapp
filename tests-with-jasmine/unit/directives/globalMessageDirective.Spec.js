describe('check work of globalMessageDirective', function() {
    beforeEach(module('myApp'));

    var element;
    var $scope;

    beforeEach(inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.globalMessage = {
            message: '',
            type: 'base',
            hide: false,
        };
        element = angular.element('<div global-message data-message="globalMessage.message"' +
            'data-type="globalMessage.type" data-hide="globalMessage.hide"></div>');

        _$compile_(element)($scope);
        $scope.$apply();

    }));

    it('should contain message', function() {
        var message = 'Hello world';
        $scope.globalMessage.message = message;
        $scope.$apply();
        expect(element.text()).toContain(message);
    });

    it('should has b-global-message_shown_error when type of message - error', function() {
        var message = 'Hello world';
        $scope.globalMessage.message = 'Error';
        $scope.globalMessage.type = 'error';
        $scope.$apply();
        expect(element.hasClass('b-global-message_shown_error')).toBeTruthy();
    });

});