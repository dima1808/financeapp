var HttpBackend = require('http-backend-proxy');
var proxy = new HttpBackend(browser);

//backend for google
proxy.onLoad.whenJSONP('http://www.google.com/finance/info?callback=JSON_CALLBACK&q=EPAM,IBM')
    .respond(200, [
        { t: 'EPAM', c: '10', cp: 10, l: 100 },
        { t: 'IBM', c: '10', cp: 10, l: 100 }
    ]);
proxy.onLoad.whenJSONP('http://www.google.com/finance/info?callback=JSON_CALLBACK&q=III')
    .respond(200, [{ t: 'III', c: '10', cp: 10, l: 100 }]);
proxy.onLoad.whenJSONP('http://www.google.com/finance/info?callback=JSON_CALLBACK&q=notExistCompany')
    .respond(404, '');

//backend for yahoo
var baseGroupCompanyNames = ['EPAM', 'IBM'];
var info = ['symbol', 'LastTradePriceOnly', 'Change_PercentChange'];
var baseQuery = 'https://query.yahooapis.com/v1/public/yql?env=' +
    encodeUriQuery('store://datatables.org/alltableswithkeys') +
    '&format=json&q=';

var baseCompanies = baseGroupCompanyNames.map(function(x) { return '\'' + x + '\'' }).join(',');
var q = encodeUriQuery('select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (' + baseCompanies + ')');

proxy.onLoad.when('GET', baseQuery + q)
    .respond(200, {
        query: {
            results: {
                quote: [{
                    symbol: baseGroupCompanyNames[0],
                    Change_PercentChange: '10 - 10%',
                    LastTradePriceOnly: 100,
                }, {
                    symbol: baseGroupCompanyNames[1],
                    Change_PercentChange: '10 - 10%',
                    LastTradePriceOnly: 100,
                }],
            },
        },
    });

/**
 * This method is intended for encoding *key* or *value* parts of query component. We need a custom
 * method because encodeURIComponent is too aggressive and encodes stuff that doesn't have to be
 * encoded per http://tools.ietf.org/html/rfc3986:
 *    query       = *( pchar / "/" / "?" )
 *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
 *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
 *    pct-encoded   = "%" HEXDIG HEXDIG
 *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
 *                     / "*" / "+" / "," / ";" / "="
 */
function encodeUriQuery(val, pctEncodeSpaces) {
    return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%3B/gi, ';').
    replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
}

module.exports = proxy;