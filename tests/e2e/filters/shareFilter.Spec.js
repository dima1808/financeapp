describe('E2E: testing shareFilter', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should all button of share-types be active', function () {
        var allBtn = element(by.css('.share-types__btns > button:first-child'));
        allBtn.getAttribute('class').then(function(value) {
            expect(value.indexOf('b-btn_primary_active') !== -1).to.be.true;
        });
    });

    it('should positive button of share-types be active', function () {
        var positiveBtn = element(by.css('.share-types__btns > button:nth-of-type(2)'));
        positiveBtn.click().then(function() {
            positiveBtn.getAttribute('class').then(function(value) {
                expect(value.indexOf('b-btn_primary_active') !== -1).to.be.true;
            });
        });
    });

    it('should show message when there is no items', function () {
        var negativeBtn = element(by.css('.share-types__btns > button:nth-of-type(3)'));
        negativeBtn.click().then(function() {
            element(by.css('.share-types__message')).getAttribute('class').then(function(value) {
                expect(value.indexOf('ng-hide') === -1).to.be.true;
            });
        });
    });

    it('should show items when positive button is clicked', function(done) {
        var positiveBtn = element(by.css('.share-types__btns > button:nth-of-type(2)'));
        positiveBtn.click().then(function() {
            var countItems = element.all(by.css('.b-finance-item')).count();
            expect(countItems).to.eventually.be.equal(2).notify(done);
        });
    });

});