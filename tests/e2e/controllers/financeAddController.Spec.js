describe('E2E: testing FinanceAddController', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';
    var input = element(by.model('financeAddCtrl.company.name'));
    var form = element(by.name('financeAddForm'));

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    before(function() {
        browser.get(basePath + '#/google');
    });

    after(function () {
        browser.executeScript("window.localStorage.clear();");
    });

    it('should add info', function(done) {

        var baseCount = element.all(by.css('.b-finance-item__head h3')).count();

        baseCount.then(function(value) {
            input.sendKeys('III');
            form.submit().then(function() {
                element.all(by.css('.b-finance-item__head h3'))
                    .then(function(items) {
                        expect(items.length).to.equal(value + 1);
                        expect(items[value].getText()).to.eventually.equal('III').notify(done);
                    });
            });
        });

    });

    it('should not add info', function(done) {

        input.clear();
        input.sendKeys('---');

        form.submit().then(function() {
            var errorMessageBlock = element(by.className('one-form-field-error-message'));
            expect(errorMessageBlock.getCssValue('display')).to.eventually.equal('block').notify(done);
        });

    });

    it('should show error message', function(done) {

        input.clear();
        input.sendKeys('notExistCompany');

        form.submit().then(function() {
            var errorMessage = element(by.css('[global-message]'));
            errorMessage.getAttribute('class')
                .then(function(classValue) {
                    expect(classValue.indexOf('b-global-message_shown_error') !== -1).to.be.true;
                    done();
                });
        });

    });

});