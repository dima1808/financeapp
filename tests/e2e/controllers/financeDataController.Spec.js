describe('E2E: testing FinanceDataController', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';
    var modal = element(by.css('.b-finance-info [modal]'));
    var financeItemBtn = element(by.css('.b-finance-item__remove button'));

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function() {
        browser.get(basePath + '#/google');
    });

    afterEach(function () {
        browser.executeScript("window.localStorage.clear();");
    });

    it('should show information about company', function(done) {
        element.all(by.css('.b-finance-item__head h3'))
            .then(function(items) {
                expect(items[0].getText()).to.eventually.equal('EPAM').notify(done);
            });
    });

    it('should show modal window', function() {
        modal.getAttribute('class').then(function(classValue) {
            expect(classValue.indexOf('ng-hide') !== -1).to.be.true;
            financeItemBtn.click();
            modal.getAttribute('class').then(function(classValue) {
                expect(classValue.indexOf('ng-hide') === -1).to.be.true;
            });
        });
    });

    it('should show and close modal window', function() {
        var modalCancelBtn = element(by.css('.b-finance-info [modal] .b-btn.b-btn_danger'));
        financeItemBtn.click();
        modalCancelBtn.click();
        modal.getAttribute('class').then(function(classValue) {
            expect(classValue.indexOf('ng-hide') !== -1).to.be.true;
        });
    });

    it('should delete information about company', function() {
        var modalConfirmBtn = element(by.css('.b-finance-info [modal] .b-btn.b-btn_primary'));
        financeItemBtn.click();
        modalConfirmBtn.click();
        var financeItem = element(by.css('.b-finance-item__head h3'));
        return expect(financeItem.getText()).not.to.eventually.equal('EPAM');
    });

});