describe('E2E: testing routes', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../baseE2E');
    var basePath = 'http://localhost:3000/';

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function() {
        browser.get(basePath);
    });

    it('should load the yahoo page', function() {
        return expect(browser.getCurrentUrl()).to.eventually.equal(basePath + "#/yahoo");
    });

    it('should load the google page', function() {
        browser.setLocation('google');
        return expect(browser.getCurrentUrl()).to.eventually.equal(basePath + "#/google");
    });

    it('should go to google page', function(done) {
        var mainLink = element(by.css('.main-nav a[href$=google]'));
        mainLink.click().then(function() {
            expect(browser.getCurrentUrl()).to.eventually.equal(basePath + "#/google").notify(done);
        });
    });

    it('should show error message', function() {
        browser.setLocation('bla');
        var alertDanger = element(by.css('.b-alert.b-alert_danger'));
        alertDanger.$('.b-alert__message').getText().then(function(value) {
            expect(value).to.equal('You have set wrong api name');
        });
        alertDanger.getAttribute('class').then(function(classValue) {
            expect(classValue.indexOf('ng-hide') == -1).to.be.true;
        });
    });

});