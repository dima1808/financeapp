describe('E2E: testing modalDirective', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should modal window exists', function () {
        return expect(element(by.className('finance-modal')).isPresent()).to.eventually.be.true;
    });

});