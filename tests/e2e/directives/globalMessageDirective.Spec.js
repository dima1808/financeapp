describe('E2E: testing globalMessageDirective', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function () {
        browser.get(basePath);
    });

    it('should global message exists', function () {
        return expect(element(by.className('b-global-message')).isPresent()).to.eventually.be.true;
    });

});