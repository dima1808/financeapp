describe('E2E: testing dragDropDirective', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var dragAndDrop = require('html-dnd').code;
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';
    var isDraggable = true;

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function () {
        browser.get(basePath);
    });

    afterEach(function () {
        browser.executeScript("window.localStorage.clear();");
    });

    it('should dragDrop element has drop-container class', function () {
        var dropElement = element(by.css('.b-finance-item-actions [drag-drop]'));
        dropElement.getAttribute('class').then(function(className) {
            expect(className.indexOf('drop-container') !== -1).to.be.true;
        });
    });

    it('should drag elements exist', function (done) {
        var dropElement = browser.findElement(by.css('.b-finance-item-actions [drag-drop]'));
        var dragElement = browser.findElement(by.css('.b-finance-item_drag-drop'));
        var modal = element(by.css('.b-finance-info [modal]'));
        browser.executeScript(dragAndDrop, dragElement, dropElement).then(function() {
            modal.getAttribute('class').then(function(classValue) {
                expect(classValue.indexOf('ng-hide') === -1).to.be.true;
                done();
            });
        });
    });

    it('should delete element by drag and drop', function (done) {
        var dropElement = browser.findElement(by.css('.b-finance-item-actions [drag-drop]'));
        var dragElement = browser.findElement(by.css('.b-finance-item_drag-drop'));
        var dragElementsCount = element.all(by.css('.b-finance-item_drag-drop')).count();
        var modalConfirmBtn = element(by.css('.b-finance-info [modal] .b-btn.b-btn_primary'));
        dragElementsCount.then(function(countElem) {
            browser.executeScript(dragAndDrop, dragElement, dropElement).then(function() {
                modalConfirmBtn.click();
                expect(element.all(by.css('.b-finance-item_drag-drop')).count()).to.eventually.equal(countElem - 1);
                done();
            });
        });
    });

});