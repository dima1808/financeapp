describe('E2E: testing financeItemDirective', function() {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');
    var proxy = require('../../baseE2E');
    var basePath = 'http://localhost:3000/';

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    beforeEach(function () {
        browser.get(basePath + '#/google');
    });

    it('should finance item exists', function () {
        return expect(element(by.className('b-finance-item')).isPresent()).to.eventually.be.true;
    });

});