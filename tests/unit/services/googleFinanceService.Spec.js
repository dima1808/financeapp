describe('check work of googleFinanceService', function() {

    var assert = chai.assert;
    var baseCompanyName = 'EPAM';
    var fakeCompanyName = 'abfdhfhrre';
    var baseGroupCompanyNames = ['EPAM', 'IBM'];
    var baseQuery = 'http://www.google.com/finance/info?callback=JSON_CALLBACK&q=';
    var $httpBackend;
    var googleFinanceService;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$injector_) {
        googleFinanceService = _$injector_.get('googleFinanceService');
        $httpBackend = _$injector_.get('$httpBackend');
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('Should return information about ' + baseCompanyName, function() {
        var response;
        $httpBackend.expect('JSONP', baseQuery + baseCompanyName)
            .respond(200, [{
                t: baseCompanyName,
                c: '10',
                cp: 10,
                l: 100
            }]);

        googleFinanceService.getAll(baseCompanyName).then(function(data) {
            response = data;
        });

        $httpBackend.flush();
        assert.strictEqual(response[0].symbol, baseCompanyName);
    });

    it('Should return information about group of companies ' + baseGroupCompanyNames.join(', '), function() {

        var response;
        $httpBackend.expect('JSONP', baseQuery + baseGroupCompanyNames.join(','))
            .respond(200, [{
                t: baseGroupCompanyNames[0],
                c: '10',
                cp: 10,
                l: 100
            }, {
                t: baseGroupCompanyNames[1],
                c: '10',
                cp: 10,
                l: 100
            }]);

        googleFinanceService.getAll(baseGroupCompanyNames).then(function(data) {
            response = data;
        });

        $httpBackend.flush();
        assert.strictEqual(response[0].symbol, baseGroupCompanyNames[0], 'must be ' + baseGroupCompanyNames[0]);
        assert.strictEqual(response[1].symbol, baseGroupCompanyNames[1], 'must be ' + baseGroupCompanyNames[1]);
    });

    it('Should not return info about wrong company', function() {

        var wrongData = false;
        $httpBackend.expect('JSONP', baseQuery + fakeCompanyName)
            .respond(404, {});

        googleFinanceService.getAll(fakeCompanyName).catch(function(data) {
            wrongData = true;
        });

        $httpBackend.flush();
        assert.isTrue(wrongData);
    });

});