describe('check work of baseService', function() {

    var assert = chai.assert;
    var baseService;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$injector_) {
        baseService = _$injector_.get('baseService');
    }));

    it('should return loading is true', function() {
        var loading = baseService.getGlobalLoading();
        assert.isTrue(loading.show, 'global loading show is true');
    });

    it('should return empty message', function() {
        var msg = baseService.getGlobalMessage();
        assert.strictEqual(msg.message, '');
    });

    it('should return that api is correct', function() {
        baseService.setCorrectApi();
        var apiSettings = baseService.getApiSettings();
        assert.isTrue(apiSettings.isCorrect);
        baseService.setErrorApi();
        apiSettings = baseService.getApiSettings();
        assert.isFalse(apiSettings.isCorrect);
    });

    it('should set correct value of globalLoading', function() {
        baseService.showGlobalLoading();
        assert.isTrue(baseService.getGlobalLoading().show);
        baseService.hideGlobalLoading();
        assert.isFalse(baseService.getGlobalLoading().show);
    });

    it('should message be correct when show and hide', function() {
        var message = 'test message';
        baseService.showMessage(message);
        assert.strictEqual(baseService.getGlobalMessage().message, message);
        baseService.showErrorMessage(message);
        assert.strictEqual(baseService.getGlobalMessage().type, 'error');
        baseService.showSuccessMessage(message);
        assert.strictEqual(baseService.getGlobalMessage().type, 'success');
    });

    it('should hide message', function() {
        baseService.hideMessage();
        assert.isTrue(baseService.getGlobalMessage().hide);
    });

});