describe('check work of financeService', function() {

    var assert = chai.assert;
    var baseCompanyName = 'EPAM';
    var financeApiConst;
    var financeService;
    var yahooFinanceService;
    var googleFinanceService;
    var $timeout;
    var $q;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$injector_) {

        financeService = _$injector_.get('financeService');
        $timeout = _$injector_.get('$timeout');
        $q = _$injector_.get('$q');
        financeApiConst = _$injector_.get('financeApiConst');
        yahooFinanceService = _$injector_.get('yahooFinanceService');
        googleFinanceService = _$injector_.get('googleFinanceService');

    }));

    it('should return name of one company', function() {
        var companyNames;
        setStubToGetAll(googleFinanceService, baseCompanyName);

        financeService.getAll(baseCompanyName, true, financeApiConst.google);

        $timeout.flush();
        companyNames = financeService.getCurrentDataNames();
        assert.strictEqual(companyNames[0], baseCompanyName);

    });

    it('should remove company from the list', function() {
        var companyNames;
        setStubToGetAll(googleFinanceService, baseCompanyName);

        financeService.getAll(baseCompanyName, true, financeApiConst.google);

        $timeout.flush();
        companyNames = financeService.getCurrentDataNames();
        assert.equal(companyNames.length, 1, 'companyNames length is 1');

        financeService.remove(baseCompanyName);

        companyNames = financeService.getCurrentDataNames();
        assert.equal(companyNames.length, 0, 'companyNames length is 0');

    });

    it('should get info about companies', function() {
        var companyNames;
        setStubToGetAll(yahooFinanceService, baseCompanyName);

        financeService.getAll(baseCompanyName, false, financeApiConst.yahoo);

        $timeout.flush();
        companyNames = financeService.getCurrentDataNames();
        assert.equal(companyNames[0], baseCompanyName);
    });

    function setStubToGetAll(service, symbol) {
        sinon.stub(service, 'getAll', function() {
            return $q(function(resolve, reject) {
                $timeout(function() {
                    resolve([{ symbol: symbol, }]);
                }, 0);
            });
        });
    }

});