describe('check work of yahooFinanceService', function() {

    var assert = chai.assert;
    var baseCompanyName = 'EPAM';
    var baseGroupCompanyNames = ['EPAM', 'IBM'];
    var $httpBackend;
    var $httpParamSerializerJQLike;
    var yahooFinanceService;
    var info = [ 'symbol', 'LastTradePriceOnly', 'Change_PercentChange' ];
    var baseQuery = 'https://query.yahooapis.com/v1/public/yql?';
    var params = {
        env: 'store://datatables.org/alltableswithkeys',
        format: 'json',
        q: '',
    };

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$injector_) {
        yahooFinanceService = _$injector_.get('yahooFinanceService');
        $httpParamSerializerJQLike = _$injector_.get('$httpParamSerializerJQLike');
        $httpBackend = _$injector_.get('$httpBackend');
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('Should return information about ' + baseCompanyName, function() {

        var baseChange = 10;
        var response;
        params.q = 'select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (\'' + baseCompanyName + '\')';

        $httpBackend.expect('GET', baseQuery + $httpParamSerializerJQLike(params))
            .respond(200, {
                query: {
                    results: {
                        quote: [{
                            symbol: baseCompanyName,
                            Change_PercentChange: baseChange + ' - 10%',
                            LastTradePriceOnly: 100
                        }]
                    },
                },
            });

        yahooFinanceService.getAll(baseCompanyName).then(function(data) {
            response = data;
        });

        $httpBackend.flush();
        assert.strictEqual(response[0].symbol, baseCompanyName);
        assert.strictEqual(response[0].change, baseChange);
    });

    it('Should return information about group of companies ' + baseGroupCompanyNames.join(', '), function() {

        var baseCompanies = baseGroupCompanyNames.map(function(x) { return '\'' + x + '\'' }).join(',');
        var basePrepareCompanies = angular.copy(baseGroupCompanyNames);
        var response;
        params.q = 'select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (' + baseCompanies + ')';

        $httpBackend.expect('GET', baseQuery + $httpParamSerializerJQLike(params))
            .respond(200, {
                query: {
                    results: {
                        quote: [{
                            symbol: baseGroupCompanyNames[0],
                            Change_PercentChange: '10 - 10%',
                            LastTradePriceOnly: 100,
                        }, {
                            symbol: baseGroupCompanyNames[1],
                            Change_PercentChange: '10 - 10%',
                            LastTradePriceOnly: 100,
                        }],
                    },
                },
            });

        yahooFinanceService.getAll(baseGroupCompanyNames).then(function(data) {
            response = data;
        });

        $httpBackend.flush();
        assert.strictEqual(response[0].symbol, basePrepareCompanies[0]);
        assert.strictEqual(response[1].symbol, basePrepareCompanies[1]);
    });

    it('Should not return info about wrong company', function() {

        var fakeCompanyName = 'notExistCompany';
        var wrongData = false;
        params.q = 'select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (\'' + fakeCompanyName + '\')';

        $httpBackend.expect('GET', baseQuery + $httpParamSerializerJQLike(params))
            .respond(404, {
                error: {
                    description: 'Wrong company',
                },
            });

        yahooFinanceService.getAll(fakeCompanyName).catch(function(data) {
            wrongData = true;
        });

        $httpBackend.flush();
        assert.isTrue(wrongData);
    });

});