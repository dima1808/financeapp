describe('check work of financeApiDataService', function() {

    var assert = chai.assert;
    var financeApiDataService;
    var financeApiConst;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$injector_) {
        financeApiDataService = _$injector_.get('financeApiDataService');
        financeApiConst = _$injector_.get('financeApiConst');
    }));

    it('should return yahoo api type', function() {
        var apiData = financeApiDataService.getApiData();
        assert.strictEqual(apiData.apiTypes[0].value, financeApiConst.yahoo);
    });

    it('should return that api type yahoo exists', function() {
        var isApi = financeApiDataService.isApi(financeApiConst.yahoo);
        assert.isTrue(isApi);
    });

    it('should return that api type bla does not exist', function() {
        var isApi = financeApiDataService.isApi('bla');
        assert.isFalse(isApi);
    });

});