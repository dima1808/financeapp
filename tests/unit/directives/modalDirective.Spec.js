describe('check work of globalMessageDirective', function() {

    var assert = chai.assert;
    var $scope;
    var testBody;
    var node;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.modal = {
            isShown: false,
        };
        $scope.hideModal = function() {
            $scope.modal.isShown = false;
        };
        testBody = '<div class="test">Test body</div>';
        var element = angular.element('<div><div class="ng-hide" modal ' +
            'data-show="modal.isShown" ' +
            'data-close="hideModal()" data-modal-title="Test modal"> ' +
            testBody +
            '</div></div>');

        node = _$compile_(element)($scope);
        $scope.$apply();
    }));

    it('should contain h4 tag', function() {
        assert.include(node.html(), '</h4>');
    });

    it('should not contain ng-hide class and contain it when isShown is false', function() {
        var modalBlock = node.children().eq(0);
        $scope.modal.isShown = true;
        $scope.$apply();
        assert.isFalse(modalBlock.hasClass('ng-hide'));
        $scope.modal.isShown = false;
        $scope.$apply();
        assert.isTrue(modalBlock.hasClass('ng-hide'));
    });

    it('should contain testBody', function() {
        assert.include(node.html(), 'Test body');
    });

    it('should contain button', function() {
        assert.include(node.html(), 'button');
    });

});