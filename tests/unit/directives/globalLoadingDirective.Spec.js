describe('check work of financeItemDirective', function() {

    var assert = chai.assert;
    var loadingClass = 'b-global-loading';
    var element;
    var $scope;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.loading = {
            show: false,
        };
        element = angular.element('<div global-loading data-show="loading.show"></div>');

        _$compile_(element)($scope);
        $scope.$apply();

    }));

    it('should contain class' + loadingClass, function() {
        assert.isFalse(element.html().indexOf(loadingClass) == -1);
    });

    it('should not has class ng-hide', function() {
        $scope.loading.show = true;
        $scope.$apply();
        assert.isFalse(element.hasClass('ng-hide'));
    });

});