describe('check work of globalMessageDirective', function() {

    var assert = chai.assert;
    var element;
    var $scope;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.globalMessage = {
            message: '',
            type: 'base',
            hide: false,
        };
        element = angular.element('<div global-message data-message="globalMessage.message"' +
            'data-type="globalMessage.type" data-hide="globalMessage.hide"></div>');

        _$compile_(element)($scope);
        $scope.$apply();

    }));

    it('should contain message', function() {
        var message = 'Hello world';
        $scope.globalMessage.message = message;
        $scope.$apply();
        assert.include(element.text(), message);
    });

    it('should has b-global-message_shown_error when type of message - error', function() {
        $scope.globalMessage.message = 'Error';
        $scope.globalMessage.type = 'error';
        $scope.$apply();
        assert.isTrue(element.hasClass('b-global-message_shown_error'));
    });

});