describe('check work of dragDropDirective', function() {

    var assert = chai.assert;
    var testClass = 'test-class';
    var element;
    var $scope;

    before(function() {
        var elementDrag = angular.element('<div></div><div class="' + testClass + '"></div><div class="' + testClass + '"></div>' +
            '<div class="' + testClass + '"></div><div class="' + testClass + '"></div></div>');
        angular.element(document.body).append(elementDrag);
    });

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.watchElement = 0;
        $scope.deleteItem = function(name) { };

        element = angular.element('<div drag-drop data-drag-elements-class="' + testClass + '"' +
        'data-drop-handler="deleteItem(name)"' +
        'data-watch-elements="{{watchElement}}"></div>');

        _$compile_(element)($scope);
        $scope.$apply();

    }));

    it('should have drop-container class', function() {
        assert.isTrue(element.hasClass('drop-container'));
    });

    it('should drag elements be draggable', function() {
        var dragElements = angular.element(document.getElementsByClassName(testClass));
        assert.isTrue(dragElements.eq(0).hasClass(testClass));
    });

    it('should be 4 drag elements', function() {
        var dragElements = angular.element(document.getElementsByClassName(testClass));
        assert.equal(dragElements.length, 4);
    });

    it('should be 5 drag elements', function() {
        var elementDrag = angular.element('<div><div class="' + testClass + '"></div></div>');
        angular.element(document.body).append(elementDrag);
        var dragElements = angular.element(document.getElementsByClassName(testClass));
        assert.equal(dragElements.length, 5);
    });

});