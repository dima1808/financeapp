describe('check work of oneFieldFormBlurDirective', function() {

    var assert = chai.assert;
    var element;
    var form;
    var input;
    var $scope;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.company = {
            name: '',
        };
        element = angular.element('<form name="testForm"><input type="text" name="testInput" one-field-form-blur' +
            ' ng-model="company.name" ng-model-options="{ updateOn: \'blur\' }"' +
            ' required ng-pattern="/^[A-Za-z0-9]*$/">' +
            '<button class="b-btn b-btn_primary"></button>' +
            '<div class="one-form-field-error-message"></div></form>');

        angular.element(document.body).append(element);
        _$compile_(element)($scope);
        $scope.$apply();
        form = $scope.testForm;
        input = element.find('input');
    }));

    it('should contain invalid data', function() {
        input.val('---');
        form.testInput.$setViewValue('---');
        input.triggerHandler('blur');
        $scope.$apply();
        assert.isTrue(element.hasClass('ng-invalid'));
        assert.equal(element[0].querySelector('.one-form-field-error-message').style.display, 'block');
    });

    it('should contain valid data', function() {
        input.val('test');
        form.testInput.$setViewValue('test');
        input.triggerHandler('blur');
        $scope.$apply();
        assert.isTrue(element.hasClass('ng-valid'));
        assert.equal(element[0].querySelector('.one-form-field-error-message').style.display, '');
    });

});