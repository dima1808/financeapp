describe('check work of financeItemDirective', function() {

    var assert = chai.assert;
    var buttonClass = 'b-btn_simple_danger';
    var element;
    var $scope;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
        $scope = _$rootScope_.$new();
        $scope.company = {
            symbol: 'test symbol',
            change: 10,
            percentChange: 10,
            lastPrice: 200,
        };
        $scope.deleteItem = function() {

        };
        element = angular.element('<div finance-item data-company="company"' +
            ' data-delete-item="deleteItem()"></div>');

        _$compile_(element)($scope);
        $scope.$apply();

    }));

    it('should contain header', function() {
        assert.isFalse(element.html().indexOf('header') == -1);
    });

    it('should contain company name in header', function() {
        assert.isFalse(element.find('header').text().indexOf($scope.company.symbol) == -1);
    });

    it('should button has ' + buttonClass, function() {
        var isClass = element.find('button').hasClass(buttonClass);
        assert.isTrue(isClass);
    });

});