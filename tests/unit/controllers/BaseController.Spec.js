describe('check work of BaseController', function() {

    var assert = chai.assert;
    var $controller;
    var baseController;
    var baseService;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$controller_, _$injector_) {
        $controller = _$controller_;
        baseService = _$injector_.get('baseService');
        sinon.stub(baseService, 'getGlobalLoading', function() {
            return true;
        });
        sinon.stub(baseService, 'getGlobalMessage', function() {
            return true;
        });
        sinon.stub(baseService, 'getApiSettings', function() {
            return true;
        });
    }));

    beforeEach(function() {
        baseController = $controller('BaseController', {
            baseService: baseService,
        });
    });

    it('should globalMessage to be defined and be true', function() {
        assert.isDefined(baseController.globalMessage);
        assert.isTrue(baseController.globalMessage);
    });

    it('should globalLoading to be defined and be true', function() {
        assert.isDefined(baseController.globalLoading);
        assert.isTrue(baseController.globalLoading);
    });

    it('should globalCorrectApi to be defined and be true', function() {
        assert.isDefined(baseController.globalCorrectApi);
        assert.isTrue(baseController.globalCorrectApi);
    });

});