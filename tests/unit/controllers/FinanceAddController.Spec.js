describe('check work of FinanceAddController', function() {

    var assert = chai.assert;
    var baseCompanyName = 'EPAM';
    var $controller;
    var baseService;
    var financeService;
    var $q;
    var $timeout;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$timeout_, _$controller_, _$injector_) {

        $controller = _$controller_;
        $q = _$injector_.get('$q');
        $timeout = _$timeout_;
        baseService = _$injector_.get('baseService');
        financeService = _$injector_.get('financeService');

        sinon.stub(financeService, 'getAll', function() {
            return $q(function(resolve, reject) {
                $timeout(function () {
                    resolve([{
                        symbol: baseCompanyName,
                    }]);
                }, 0);
            });
        });

    }));

    it('should set empty company name after data is added', function() {
        sinon.stub(financeService, 'getCurrentDataNames', function() {
            return [];
        });
        var financeAddController = $controller('FinanceAddController', {
            baseService: baseService,
            financeService: financeService,
        });
        financeAddController.company.name = baseCompanyName;
        financeAddController.addFinanceInfo({$valid: true});
        $timeout.flush();
        assert.strictEqual(financeAddController.company.name, '');
    });

    it('should not add data and not clear company name', function() {
        var financeAddController = $controller('FinanceAddController', {
            baseService: baseService,
            financeService: financeService,
        });
        financeAddController.company.name = baseCompanyName;
        financeAddController.addFinanceInfo({$valid: false});
        assert.strictEqual(financeAddController.company.name, baseCompanyName);
    });

    it('should not add data when company already exists', function() {
        sinon.stub(financeService, 'getCurrentDataNames', function() {
            return [baseCompanyName];
        });
        var financeAddController = $controller('FinanceAddController', {
            baseService: baseService,
            financeService: financeService,
        });
        financeAddController.company.name = baseCompanyName;
        financeAddController.addFinanceInfo({$valid: true});
        try {
            $timeout.flush();
        }
        catch(e) { }
        assert.strictEqual(financeAddController.company.name, baseCompanyName);
    });

});