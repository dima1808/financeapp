describe('check work of FinanceVariantController', function() {

    var assert = chai.assert;
    var $controller;
    var financeApiDataService;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$controller_, _$injector_) {
        $controller = _$controller_;
        financeApiDataService = _$injector_.get('financeApiDataService');
        sinon.stub(financeApiDataService, 'getApiData', function() {
            return {
                test: 'test',
            };
        });
    }));

    it('should get correct api data', function() {
        financeVariantController = $controller('FinanceVariantController', {
            financeApiDataService: financeApiDataService,
        });
        var apiData = financeVariantController.apiItems;
        assert.strictEqual(apiData.test, 'test');
    });

});