describe('check work of FinanceDataController', function() {

    var assert = chai.assert;
    var baseCompanyName = 'EPAM';
    var $controller;
    var financeApiDataService;
    var baseService;
    var financeService;
    var $scope;
    var $q;
    var $timeout;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$timeout_, _$controller_, _$rootScope_, _$injector_) {

        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $q = _$injector_.get('$q');
        $timeout = _$timeout_;
        financeApiDataService = _$injector_.get('financeApiDataService');
        financeService = _$injector_.get('financeService');
        baseService = _$injector_.get('baseService');

        sinon.stub(financeService, 'getAll', function() {
            return $q(function(resolve, reject) {
                $timeout(function () {
                    resolve([{
                        symbol: baseCompanyName,
                    }]);
                }, 0);
            });
        });

    }));

    it('should contains information about companies', function() {
        var financeDataController = getFinanceDataController('');
        $timeout.flush();
        assert.equal(financeDataController.companiesInfo.length, 1);
        assert.strictEqual(financeDataController.companiesInfo[0].symbol, baseCompanyName);
        assert.isFalse(financeDataController.companiesData.isEmpty);
    });

    it('should set error api', function() {
        var financeDataController = getFinanceDataController('wrongApi');
        assert.isFalse(baseService.getApiSettings().isCorrect);
    });

    it('should show and hide modal window', function() {
        var financeDataController = getFinanceDataController('');
        $timeout.flush();
        financeDataController.prepareDeleteCompanyInfo(baseCompanyName);
        assert.isTrue(financeDataController.deleteCompanyModalWindow.isShown);
        financeDataController.cancelDeleteCompanyInfo();
        assert.isFalse(financeDataController.deleteCompanyModalWindow.isShown);
    });

    it('should set and check current share type', function() {
        var financeDataController = getFinanceDataController('');
        $timeout.flush();
        financeDataController.setShareType(1);
        assert.isTrue(financeDataController.isActiveShareType(1));
    });

    function getFinanceDataController(apiName) {
        return $controller('FinanceDataController', {
            $scope: $scope,
            $stateParams: { apiName: apiName },
            financeApiDataService: financeApiDataService,
            baseService: baseService,
            financeService: financeService,
        });
    }

});