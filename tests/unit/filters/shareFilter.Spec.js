describe('check work of shareFilter', function() {

    var assert = chai.assert;
    var $filter;

    beforeEach(angular.mock.module('myApp'));

    beforeEach(angular.mock.inject(function(_$filter_) {
        $filter = _$filter_;
    }));

    it('should return only shares with negative change', function() {
        var dataBefore = [
            { change: 2 },
            { change: 2.22 },
            { change: -1 },
        ];
        var dataAfter = $filter('share')(dataBefore, -1);
        assert.equal(dataAfter.length, 1);
    });

    it('should return all shares', function() {
        var dataBefore = [
            { change: 2 },
            { change: 2.22 },
            { change: -1 },
        ];
        var dataAfter = $filter('share')(dataBefore, 0);
        assert.equal(dataAfter.length, dataBefore.length);
    });

    it('should return only shares with positive change', function() {
        var dataBefore = [
            { change: 2 },
            { change: -1 },
            { change: 3.33 },
        ];
        var dataAfter = $filter('share')(dataBefore, 1);
        assert.equal(dataAfter.length, 2);
        assert.equal(dataAfter[1].change, dataBefore[2].change);
    });

});