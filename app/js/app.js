(function() {

    'use strict';

    angular.module('myApp', ['ui.router', 'templates', 'myApp.config'])
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.when('', 'yahoo');

        $stateProvider
            .state('apiFinance', {
                url: '/:apiName',
                templateUrl: 'views-financeInfo.html',
                controller: 'FinanceDataController',
                controllerAs: 'financeDataCtrl',
            });

    }

}());