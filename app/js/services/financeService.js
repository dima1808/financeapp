(function() {

    'use strict';

    angular.module('myApp')
        .service('financeService', financeService);

    financeService.$inject = ['financeApiConst', 'financeApiDataService', 'yahooFinanceService', 'googleFinanceService'];

    //gets and saves data from different api
    function financeService(financeApiConst, financeApiDataService, yahooFinanceService, googleFinanceService) {

        //webStorageCompaniesName - key of current companies in the localStorage
        var webStorageCompaniesName = 'currentCompanies';

        //current data of companies
        var currentData = [];

        this.getAll = getAll;
        this.remove = remove;
        this.getCurrentDataNames = getCurrentDataNames;

        init();

        function init() {
            var currentCompanies = getCurrentStorageData();

            if(currentCompanies && currentCompanies.length) {
                currentData = currentCompanies;
            }
        }

        /**
         * Get data of api
         *
         * @method getData
         * @param api {string} the name of api, e.g. 'yahoo'
         * @param data {array} the array of the names of companies
         * @returns promise
         */
        function getData(api, data) {
            var promise;

            if(api === financeApiConst.yahoo) {
                promise = yahooFinanceService.getAll(data);
            }
            else if(api === financeApiConst.google) {
                promise = googleFinanceService.getAll(data);
            }

            return promise;
        }

        /**
         * Set information about companies and add this information to webStorage
         *
         * @method createData
         * @param data {array} the array of objects that contain information about companies
         * @param replace {boolean} if true than we need to clear old data, if false than we just need to add new data to old data
         */
        function createData(data, replace) {
            if(replace) {
                currentData.splice(0, currentData.length);
            }

            data.forEach(function(value) {
                currentData.push(value);
            });

            setCurrentStorageData();
        }

        /**
         * Get info about companies
         *
         * @method getAll
         * @param data {array} the array of the names of companies
         * @param replace {boolean} if true than we need to clear old data
         * @param api {string} the name of api, e.g. 'yahoo' (optional)
         *                      If it is not set - it will be current api
         * @returns promise
         */
        function getAll(data, replace, api) {

            if(api === undefined) {
                api = financeApiDataService.getApiData().currentApi;
            }

            return getData(api, data).then(function(data) {
                createData(data, replace);
                return currentData;
            });

        }

        /**
         * Remove info about chosen company
         *
         * @method @remove
         * @param name {string} the name of the current company
         */
        function remove(name) {
            var index;

            for(var i = 0, lh = currentData.length; i < lh; i++) {
                if(currentData[i].symbol.toUpperCase() === name.toUpperCase()) {
                    index = i;
                    break;
                }
            }

            if(index !== undefined) {
                currentData.splice(index, 1);
                setCurrentStorageData();
            }
        }

        /**
         * Get list of names of current companies
         *
         * @method getCurrentDataNames
         * @returns {Array} names of current companies
         */
        function getCurrentDataNames() {
            var names = [];

            currentData.forEach(function(value) {
               names.push(value.symbol);
            });

            return names;
        }

        function setCurrentStorageData() {
            localStorage.setItem(webStorageCompaniesName, JSON.stringify(currentData));
        }

        function getCurrentStorageData() {
            return JSON.parse(localStorage.getItem(webStorageCompaniesName));
        }

    }

}());