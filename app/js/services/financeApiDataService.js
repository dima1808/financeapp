(function() {

    'use strict';

    angular.module("myApp")
        .service("financeApiDataService", financeApiDataService);

    financeApiDataService.$inject = ["financeApiConst"];

    /**
     * Contains information about api types
     *
     * @param financeApiConst variants of api, e.g. 'yahoo'
     */
    function financeApiDataService(financeApiConst) {

        //apiData - all api variants and current chosen api
        var apiData = {
            currentApi: financeApiConst.yahoo,
            apiTypes: [{
                value: financeApiConst.yahoo,
                name: 'Yahoo API',
            }, {
                value: financeApiConst.google,
                name: 'Google API',
            }],
        };

        this.getApiData = getApiData;
        this.isApi = isApi;

        function getApiData() {
            return apiData;
        }

        /**
         *  Check if there is api with value apiType
         *
         * @param apiType {string} the value of api, e.g. 'yahoo'
         * @returns {boolean} returns true if there is such api
         */
        function isApi(apiType) {
            var isApiType = false;

            for(var i = 0, lh = apiData.apiTypes.length; i < lh; i++) {
                if(apiData.apiTypes[i].value.toLowerCase() === apiType.toLowerCase()) {
                    isApiType = true;
                    break;
                }
            }

            return isApiType;
        }

    }

}());