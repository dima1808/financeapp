(function() {

    'use strict';

    angular.module('myApp')
        .service('yahooFinanceService', yahooFinanceService);

    yahooFinanceService.$inject = ['$http', '$q'];

    //service works with yahoo api
    function yahooFinanceService($http, $q) {

        var basePath = 'https://query.yahooapis.com/v1/public/yql';
        var baseFormat = 'json';
        var baseEnv = 'store://datatables.org/alltableswithkeys';
        var wrongCompaniesNameMessage = 'There is no data about companies you have entered';

        /**
         * Parameters from api
         *
         * @property info
         * @type {string[]} an array of data from api
         */
        var info = [
            'symbol',
            'LastTradePriceOnly',
            'Change_PercentChange',
        ];

        this.getAll = getAll;

        /**
         * Get information about companies from api
         *
         * @method getAll
         * @param data {Array|string} an array of names of companies or name of one company
         * @returns {promise}
         */
        function getAll(data) {

            var config = {
                method: 'GET',
                url: basePath,
                params: {
                    q: 'select ' + info.join(', ') + ' from yahoo.finance.quotes where symbol in (' + prepareData(data) + ')',
                    format: baseFormat,
                    env: baseEnv
                },
            };

            return $q(function(resolve, reject) {

                $http(config).success(function(data, status) {

                    var currentData = createResult(data);

                    if(currentData) {
                        resolve(currentData);
                    }
                    else {
                        reject({
                            errorMessage: wrongCompaniesNameMessage,
                        }, status);
                    }

                }).error(function(data) {

                    reject({
                        errorMessage: data.error.description,
                    });

                });

            });

        }

        /**
         * Prepare data before set that data in query
         *
         * @method prepareData
         * @param data {Array|string} array of companies or just name of one company
         * @returns {string} string of the names of companies
         */
        function prepareData(data) {
            if(angular.isArray(data)) {
                for(var i = 0, lh = data.length; i < lh; i++) {
                    data[i] = "'" + data[i] + "'";
                }

                return data.join(',');
            }

            return "'" + data + "'";
        }

        /**
         *  Create information about companies
         *
         * @method createResult
         * @param data {Array|Object} An array of objects or object that contains information about companies (company)
         * @returns {Array|false} array if there is information about companies otherwise return false
         */
        function createResult(data) {
            var currentData = [];
            var isResult = false;
            var financeData;

            if(data.query && data.query.results && data.query.results.quote) {
                financeData = data.query.results.quote;
                if(!angular.isArray(financeData) && financeData[info[1]] != null) {
                    isResult = true;
                    currentData.push(createResultData(financeData));
                }
                else if(angular.isArray(financeData)) {
                    financeData.forEach(function(value) {
                        if(value[info[1]] != null) {
                            isResult = true;
                            currentData.push(createResultData(value));
                        }
                    })
                }
            }

            return isResult ? currentData : false;
        }

        /**
         * Create an information about one company
         *
         * @method createResultData
         * @param value {Object} an information about company from api
         * @returns {Object}
         */
        function createResultData(value) {
            var data = {};
            var changeData = value[info[2]] == null ? [0, 0] : value[info[2]].split(' - ');

            data.symbol = value[info[0]].toUpperCase();
            data.change = parseFloat(changeData[0]);
            data.percentChange = parseFloat(changeData[1]);
            data.lastPrice = parseFloat(value[info[1]]);

            return data;
        }

    }

}());