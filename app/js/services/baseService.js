(function() {

    'use strict';

    angular.module('myApp')
        .service('baseService', baseService);

    /**
     * Contains information about global parameters
     * such as global message state and etc.
     */
    function baseService() {

        var loading = {
            show: true,
        };

        var globalMessage = {
            message: '',
            type: 'base',
            hide: false,
        };

        //if it is wrong api than isCorrect will be false
        var apiSettings = {
            isCorrect: true,
        };

        this.getGlobalLoading = getGlobalLoading;
        this.showGlobalLoading = showGlobalLoading;
        this.hideGlobalLoading = hideGlobalLoading;
        this.getGlobalMessage = getGlobalMessage;
        this.showMessage = showMessage;
        this.showErrorMessage = showErrorMessage;
        this.showSuccessMessage = showSuccessMessage;
        this.hideMessage = hideMessage;
        this.getApiSettings = getApiSettings;
        this.setErrorApi = setErrorApi;
        this.setCorrectApi = setCorrectApi;

        function getGlobalLoading() {
            return loading;
        }

        function showGlobalLoading() {
            loading.show = true;
        }

        function hideGlobalLoading() {
            loading.show = false;
        }

        function getGlobalMessage() {
            return globalMessage;
        }

        function showMessage(message) {
            globalMessage.message = message;
            globalMessage.type = 'base';
        }

        function showErrorMessage(message) {
            globalMessage.message = message;
            globalMessage.type = 'error';
        }

        function showSuccessMessage(message) {
            globalMessage.message = message;
            globalMessage.type = 'success';
        }

        function hideMessage() {
            globalMessage.hide = true;
        }

        function getApiSettings() {
            return apiSettings;
        }

        function setErrorApi() {
            apiSettings.isCorrect = false;
        }

        function setCorrectApi() {
            apiSettings.isCorrect = true;
        }

    }

}());