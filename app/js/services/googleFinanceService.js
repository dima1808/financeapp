(function() {

    'use strict';

    angular.module('myApp')
        .service('googleFinanceService', googleFinanceService);

    googleFinanceService.$inject = ['$http', '$q'];

    //service works with google api
    function googleFinanceService($http, $q) {

        var basePath = 'http://www.google.com/finance/info';
        var wrongCompaniesNameMessage = 'There is no data about companies you have entered';

        this.getAll = getAll;

        /**
         * get info about chosen companies from api
         *
         * @method getAll
         * @param data {Array|string} array of names of companies or name of one company
         * @returns {promise}
         */
        function getAll(data) {

            var config = {
                method: 'JSONP',
                url: basePath,
                params: {
                    q: prepareData(data),
                    callback: 'JSON_CALLBACK',
                },
            };

            return $q(function(resolve, reject) {
                $http(config).success(function(data, status) {

                    var currentData = createResult(data);

                    if(currentData) {
                        resolve(currentData);
                    }
                    else {
                        reject({
                            errorMessage: wrongCompaniesNameMessage,
                        }, status);
                    }

                }).error(function(data) {

                    reject({
                        errorMessage: wrongCompaniesNameMessage,
                    });

                });
            });

        }

        /**
         * prepare data before set them to query
         *
         * @method prepareData
         * @param data {array|string} name or array of names of companies
         * @returns {string} string of names
         */
        function prepareData(data) {
            if(angular.isArray(data)) {
                return data.join(',');
            }

            return data;
        }

        /**
         * create result for companies
         *
         * @method createResult
         * @param data {Array} data from api
         * @returns {Array|false} array of objects of data about companies
         */
        function createResult(data) {
            var currentData = [];

            if(angular.isArray(data)) {
                data.forEach(function(value) {
                    currentData.push(createResultData(value));
                });
            }

            return currentData.length ? currentData : false;
        }

        /**
         * Create an information about company
         *
         * @method createResultData
         * @param value {Object} base information from api
         * @returns {Object} information about company
         */
        function createResultData(value) {
            var data = {};
            var change = parseFloat(value['c']);
            var percentChange = parseFloat(value['cp']);
            data.symbol = value['t'].toUpperCase();
            data.change = isNaN(change) ? 0 : change;
            data.percentChange = isNaN(percentChange) ? 0 : percentChange;
            data.lastPrice = parseFloat(value['l']);
            return data;
        }

    }

}());