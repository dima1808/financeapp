(function() {

    'use strict';

    angular.module('myApp')
        .filter('share', shareFilter);

    function shareFilter() {
        return function(items, shareType) {
            if(!shareType) {
                return items;
            }
            else if(shareType === 1) {
                return items.filter(function(item){ return item.change >= 0 });
            }
            else if(shareType === -1) {
                return items.filter(function(item) { return item.change < 0 });
            }
        }
    }

})();