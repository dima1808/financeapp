(function() {

    'use strict';

    angular.module('myApp')
        .controller('FinanceAddController', FinanceAddController);

    FinanceAddController.$inject = ['financeService', 'baseService'];

    //controller works with form that adds new companies
    function FinanceAddController(financeService, baseService) {

        /* jshint validthis: true */
        var vm = this;
        var existCompanyMessage = 'Such company is already exists';

        vm.company = {
            name: '',
        };

        vm.addFinanceInfo = addFinanceInfo;

        //if data is valid than show loading and add data
        function addFinanceInfo(form) {
            if(form.$valid && !checkIfCompanyExists()) {
                baseService.hideMessage();
                baseService.showGlobalLoading();
                var promise = financeService.getAll(vm.company.name);
                promise.then(addFinanceInfoSuccess).catch(addFinanceInfoError);
            }
        }

        function addFinanceInfoSuccess(data) {
            setDefaultCompanyInfo();
            baseService.hideGlobalLoading();
        }

        function addFinanceInfoError(data) {
            console.error(data);
            baseService.hideGlobalLoading();
            baseService.showErrorMessage(data.errorMessage);
        }

        function setDefaultCompanyInfo() {
            vm.company.name = '';
        }

        function checkIfCompanyExists() {
            var currentCompanies = financeService.getCurrentDataNames();

            if(currentCompanies.indexOf(vm.company.name.toUpperCase()) === -1) {
                return false;
            }

            baseService.showErrorMessage(existCompanyMessage);

            return true;
        }

    }

}());