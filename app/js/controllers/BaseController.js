(function() {

    'use strict';

    angular.module('myApp')
        .controller('BaseController', BaseController);

    BaseController.$inject = ['baseService'];

    //controller works with base data, e.g. global message
    function BaseController(baseService) {

        /* jshint validthis: true */
        var vm = this;

        init();

        function init() {
            vm.globalLoading = baseService.getGlobalLoading();
            vm.globalMessage = baseService.getGlobalMessage();
            vm.globalCorrectApi = baseService.getApiSettings();
        }

    }

}());