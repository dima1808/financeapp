(function() {

    'use strict';

    angular.module('myApp')
        .controller('FinanceDataController', FinanceDataController);

    FinanceDataController.$inject = ['$scope', '$stateParams', 'financeService', 'financeApiDataService', 'baseService'];

    //controller that shows an information about companies
    function FinanceDataController($scope, $stateParams, financeService, financeApiDataService, baseService) {

        /* jshint validthis: true */
        var vm = this;
        var baseCompanies = ['EPAM', 'IBM'];
        var deleteCompanyName = '';

        //vm.companiesData.shareType can be 0, 1 or -1
        vm.companiesData = {
            isEmpty: false,
            shareType: 0,
        };

        //current companies list
        vm.companiesInfo = [];

        vm.deleteCompanyModalWindow = {
            isShown: false,
        };

        //delete company info functions
        vm.prepareDeleteCompanyInfo = prepareDeleteCompanyInfo;
        vm.deleteCompanyInfo = deleteCompanyInfo;
        vm.cancelDeleteCompanyInfo = cancelDeleteCompanyInfo;
        vm.setShareType = setShareType;
        vm.isActiveShareType = isActiveShareType;

        $scope.$watch('financeDataCtrl.companiesInfo.length', function(newValue, oldValue) {
            checkIsEmpty();
            if(newValue > oldValue) {
                setShareType(0);
            }
        });

        init();

        function init() {

            //show loading before get result
            baseService.showGlobalLoading();

            //get api data and set current api value
            var apiData = financeApiDataService.getApiData();
            var apiVariant = $stateParams.apiName;

            if(apiVariant === '') {
                apiVariant = apiData.apiTypes[0].value;
            }
            else if(!financeApiDataService.isApi(apiVariant)) {
                baseService.hideGlobalLoading();
                baseService.setErrorApi();
                return;
            }

            baseService.setCorrectApi();
            apiData.currentApi = apiVariant;

            /**
             * if list of current companies is not empty then show info about that companies,
             * otherwise show info about base companies
             */
            var currentCompanies = financeService.getCurrentDataNames();
            var companiesToShow = currentCompanies.length ? currentCompanies : baseCompanies;

            getFinanceData(apiVariant, companiesToShow);
        }

        //get finance data
        function getFinanceData(type, data) {
            var promise = financeService.getAll(data, true, type);
            promise.then(getFinanceDataSuccess).catch(getFinanceDataError);
        }

        function getFinanceDataSuccess(data) {
            vm.companiesInfo = data;
            baseService.hideGlobalLoading();
        }

        function getFinanceDataError(data) {
            console.error(data);
            baseService.hideGlobalLoading();
        }

        //delete company (show modal window to confirm deletion)
        function prepareDeleteCompanyInfo(name) {
            deleteCompanyName = name;
            showDeleteCompanyModalWindow();
        }

        function deleteCompanyInfo() {
            financeService.remove(deleteCompanyName);
            finishDelete();
        }

        function cancelDeleteCompanyInfo() {
            finishDelete();
        }

        function finishDelete() {
            hideDeleteCompanyModalWindow();
            deleteCompanyName = '';
        }

        //display empty message if there is no information about companies
        function checkIsEmpty() {
            vm.companiesData.isEmpty = vm.companiesInfo.length ? false : true;
        }

        function setShareType(type) {
            vm.companiesData.shareType = type;
        }

        function isActiveShareType(type) {
            return vm.companiesData.shareType == type;
        }

        //functions to show and hide modal window
        function showDeleteCompanyModalWindow() {
            vm.deleteCompanyModalWindow.isShown = true;
        }

        function hideDeleteCompanyModalWindow() {
            vm.deleteCompanyModalWindow.isShown = false;
        }

    }

}());