(function() {

    'use strict';

    angular.module('myApp')
        .controller('FinanceVariantController', FinanceVariantController);

    FinanceVariantController.$inject = ['financeApiDataService'];

    //controller shows variants of api
    function FinanceVariantController(financeApiDataService) {

        /* jshint validthis: true */
        var vm = this;

        vm.apiItems = {};

        init();

        function init() {
            vm.apiItems = financeApiDataService.getApiData();
        }

    }

}());