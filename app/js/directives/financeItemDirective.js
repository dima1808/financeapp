(function() {

    'use strict';

    angular.module('myApp')
        .directive('financeItem', financeItemDirective);

    /**
     * directive for one item of finance data
     *
     * @returns {Object}
     */
    function financeItemDirective() {

        return {

            restrict: 'AE',
            replace: true,
            templateUrl: 'templates-financeItem.html',

            scope: {
                company: '=',
                deleteItem: '&',
            },

        };

    }

}());