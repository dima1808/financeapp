(function() {

    'use strict';

    angular.module('myApp')
        .directive('globalLoading', globalLoadingDirective);

    /**
     * directive for global loading
     *
     * @returns {Object}
     */
    function globalLoadingDirective() {

        return {

            restrict: 'AE',
            replace: true,
            templateUrl: 'templates-globalLoading.html',

            scope: {
                isShown: '=show',
            },

        };

    }

}());