(function() {

    'use strict';

    angular.module('myApp')
        .directive('globalMessage', globalMessageDirective);

    /**
     * directive that shows global message
     *
     * @returns {Object}
     */
    function globalMessageDirective() {

        //baseClasses.showMessage is class, that id added when message is shown
        var baseClasses = {
            showMessage: '_shown',
        };

        //type of message
        var baseViewClasses = {
            base: '_base',
            success: '_success',
            error: '_error',
        };

        var baseShowTime = 5000;

        return {

            restrict: 'EA',
            replace: true,
            templateUrl: 'templates-globalMessage.html',

            scope: {
                message: '=',
                type: '=',
                showTime: '@',
                hide: '=',
            },

            link: function($scope, $element, $attrs) {

                var messageClass = $attrs.baseclassname;

                var closeElements = $element.find('button');

                //string of classes that need to be removed when hide message
                var removingClasses = messageClass + baseClasses.showMessage + ' ' +
                        messageClass + baseClasses.showMessage + baseViewClasses.base + ' ' +
                        messageClass + baseClasses.showMessage + baseViewClasses.success + ' ' +
                        messageClass + baseClasses.showMessage + baseViewClasses.error;

                //true if mouse is hover on message element
                var onHover = false;

                //true if it's time to hide message
                var isTime = false;

                var tm;

                //do not close message when mouse is hover on message element
                $element.on('mouseenter', function(event) {
                    event.stopPropagation();
                    onHover = true;
                });

                //if it's already time, hide message block
                $element.on('mouseleave', function(event) {
                    event.stopPropagation();
                    onHover = false;
                    if(isTime) {
                        hideMessage();
                        $scope.$apply();
                    }
                });

                closeElements.on('click', function() {
                    close();
                    $scope.$apply();
                });

                //if message has changed then clear timeout and show new message
                $scope.$watch('message', function(newValue, oldValue) {
                    if(newValue !== '') {
                        if(tm) {
                            clearTimeout(tm);
                            $element.removeClass(removingClasses);
                        }
                        showMessage();
                    }
                });

                //if hide = true than hide the message
                $scope.$watch('hide', function(newValue, oldValue) {
                    if(newValue) {
                        $scope.hide = false;
                        close();
                    }
                });

                function showMessage() {

                    $element.addClass(messageClass + baseClasses.showMessage)
                        .addClass(messageClass + baseClasses.showMessage + (baseViewClasses[$scope.type] ? baseViewClasses[$scope.type] : baseViewClasses.base));

                    tm = setTimeout(hideMessageOnTimeout, $scope.showTime ? Number($scope.showTime) : baseShowTime);

                }

                function hideMessageOnTimeout() {
                    isTime = true;
                    if(!onHover) {
                        hideMessage();
                        $scope.$apply();
                    }
                }

                function hideMessage() {
                    $scope.message = '';
                    if($scope.type) {
                        $scope.type = 'base';
                    }
                    $element.removeClass(removingClasses);
                    isTime = false;
                }

                function close() {
                    if(tm) {
                        clearTimeout(tm);
                    }
                    hideMessage();
                }

            },

        };

    }

}());