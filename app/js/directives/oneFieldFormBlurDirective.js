(function() {

    'use strict';

    angular.module('myApp')
        .directive('oneFieldFormBlur', oneFieldFormBlurDirective);

    /**
     * directive that checks if value is valid
     * and if it is not than shows error message
     */
    function oneFieldFormBlurDirective() {

        var elementClass = 'one-form-field-invalid';

        return {

            restrict: 'A',
            require: ['ngModel', '^form'],

            scope: {},

            link: function($scope, $element, $attrs, $ctrl) {

                var ngModelController = $ctrl[0];
                var formController = $ctrl[1];
                var form = document.querySelector('form[name=' + formController.$name + ']');
                var errorBlock = form.querySelector('.one-form-field-error-message');

                showHideErrorBlock('none');

                //if form is not valid than show error message
                angular.element(form).on('submit', function() {
                    if(ngModelController.$invalid) {
                        $element.addClass(elementClass);
                        showHideErrorBlock('block');
                    }
                });

                $element.on('blur', function() {
                    $scope.$evalAsync(checkElement);
                });

                $element.on('keyup', function(event) {
                    clearElement(event);
                    $scope.$apply();
                });

                //check if element is valid and add or remove class and show or hide mistake message
                function checkElement() {
                    if(ngModelController.$valid || !$element.val()) {
                        $element.removeClass(elementClass);
                        showHideErrorBlock('none');
                    }
                    else {
                        $element.addClass(elementClass);
                        showHideErrorBlock('block');
                    }
                }

                //show or hide message block (display can be 'none' or 'block')
                function showHideErrorBlock(display) {
                    if(errorBlock) {
                        errorBlock.style.display = display;
                    }
                }

                //if is pressed 'esc' than remove text from input
                function clearElement(event) {
                    if(event.which == 27) {
                        ngModelController.$setViewValue('');
                        $element.val('');
                        $element.removeClass(elementClass);
                        showHideErrorBlock('none');
                        event.target.blur();
                    }
                }

            }

        }

    }

}());