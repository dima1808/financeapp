(function() {

    'use strict';

    angular.module('myApp')
        .directive('modal', modalDirective);

    /**
     * Directive that shows modal window
     *
     * @returns {Object}
     */
    function modalDirective() {

        return {

            restrict: 'AE',
            replace: true,
            transclude: true,
            templateUrl: 'templates-modal.html',

            scope: {
                isShown: '=show',
                header: '@modalTitle',
                close: '&',
                notNeedToCloseByEvent: '@',
            },

            link: function($scope, $element, $attrs) {

                if($scope.notNeedToCloseByEvent === 'true') {
                    return;
                }

                $scope.$watch('isShown', createKeyNav);

                //if modal window is shown than add event listener to close by esc
                function createKeyNav(newValue, oldValue) {
                    if(newValue) {
                        document.addEventListener('keyup', closeWindow, false);
                    }
                    else {
                        document.removeEventListener('keyup', closeWindow, false);
                    }
                }

                function closeWindow(event) {
                    if(event.which == 27) {
                        $scope.close();
                        $scope.$apply();
                    }
                }

            },

        };

    }

}());