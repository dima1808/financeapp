(function() {

    angular.module('myApp')
        .directive('dragDrop', dragDropDirective);

    function dragDropDirective() {

        var baseClasses = {
            dropContainer: 'drop-container',
            dropContainerActive: 'drop-container_active',
            dropContainerHover: 'drop-container_hover',
            dragActivePart: '_active',
        };

        return {
            restrict: 'AE',
            scope: {
                dragElementsClass: '@',
                dropHandler: '&',
                watchElements: '@',
            },
            link: function($scope, $element, $attrs) {

                var isDrop = false;
                var dragElements;
                var currentElem;
                var currentMarker;

                if(!('draggable' in document.createElement('span'))) {
                    $element.remove();
                    $scope.$destroy();
                }
                else {

                    $scope.$watch('watchElements', function(newValue, oldValue) {
                        selectDragElements();
                    });

                    init();

                    function init() {
                        $element.addClass(baseClasses.dropContainer);
                        $element.on('dragenter', dragEnterHandler);
                        $element.on('dragleave', dragLeaveHandler);
                        $element.on('dragover', dragOverHandler);
                        $element.on('drop', dropHandler);
                    }

                }

                function selectDragElements() {
                    dragElements = angular.element(document.getElementsByClassName($scope.dragElementsClass));
                    if(dragElements.length) {
                        dragElements.attr('draggable', 'true');
                        dragElements.on('dragstart', dragStartHandler);
                        dragElements.on('dragend', dragEndHandler);
                    }
                }

                function dragStartHandler(event) {
                    currentElem = angular.element(this);
                    currentElem.addClass($scope.dragElementsClass + baseClasses.dragActivePart);
                    currentMarker = currentElem.attr('data-marker');
                    $element.addClass(baseClasses.dropContainerActive);
                    event.dataTransfer.dropEffect = 'move';
                }

                function dragEnterHandler(event) {
                    event.preventDefault();
                    $element.addClass(baseClasses.dropContainerHover);
                }

                function dragLeaveHandler(event) {
                    $element.removeClass(baseClasses.dropContainerHover);
                }

                function dragOverHandler(event) {
                    event.preventDefault();
                }

                function dropHandler(event) {
                    isDrop = true;
                }

                function dragEndHandler(event) {
                    if(isDrop) {
                        $scope.dropHandler({name: currentMarker});
                        $scope.$apply();
                    }
                    currentElem.removeClass($scope.dragElementsClass + baseClasses.dragActivePart);
                    isDrop = false;
                    currentMarker = null;
                    $element.removeClass(baseClasses.dropContainerHover);
                    $element.removeClass(baseClasses.dropContainerActive);
                    $scope.$evalAsync(function() {
                        currentElem = null;
                    });
                }

            }
        }

    }

}());