'use strict';

let gulp = require('gulp');
let gulpConcatCss = require('gulp-concat-css');
let gulpConcat = require('gulp-concat');
let gulpRename = require('gulp-rename');
let gulpMinifyCss = require('gulp-minify-css');
let gulpAutoprefixer = require('gulp-autoprefixer');
let gulpSass = require('gulp-sass');
let gulpPlumber = require('gulp-plumber');
let gulpHtmlMin = require('gulp-htmlmin');
let gulpUglify = require('gulp-uglify');
let gulpAngularTemplateCache = require('gulp-angular-templatecache');
let gulpSourcemaps = require('gulp-sourcemaps');
let gulpConfig = require('gulp-ng-config');
let connect = require('gulp-connect');
let Server = require('karma').Server;
let protractor = require('gulp-protractor').protractor;
let webdriver_update = require('gulp-protractor').webdriver_update;
let webdriver_standalone = require("gulp-protractor").webdriver_standalone;

const PATHS = {
        BUILD_HTML: 'app',
        DEST_HTML: 'dist',
        BUILD_HTML_TEMPLATES: 'app/templates',
        BUILD_HTML_VIEWS: 'app/views',
        BUILD_CSS: 'app/css',
        DEST_CSS: 'dist/css',
        SASS: 'app/scss',
        CSS_FROM_SASS: 'app/css-from-sass',
        BUILD_JS: 'app/js',
        DEST_JS: 'dist/js',
    };
const FILES = {
        BUILD_CSS_BASE: PATHS.BUILD_CSS + '/base/*.css',
        BUILD_CSS_VENDOR: PATHS.BUILD_CSS + '/vendor/*.css',
        BUILD_SASS: PATHS.SASS + '/**/*.scss',
        BUILD_HTML: PATHS.BUILD_HTML + '/*.html',
        BUILD_HTML_TEMPLATES: PATHS.BUILD_HTML_TEMPLATES + '/*.html',
        BUILD_HTML_VIEWS: PATHS.BUILD_HTML_VIEWS + '/*html',
        BUILD_JS_PARTS: [
            PATHS.BUILD_JS + '/config.js',
            PATHS.BUILD_JS + '/templates.js',
            PATHS.BUILD_JS + '/app.js',
            PATHS.BUILD_JS + '/**/*.js',
        ],
        CONFIG_JSON: 'config.json',
        MAIN_CSS: 'main.css',
        MAIN_MIN_CSS: 'main.min.css',
        MAIN_MIN_JS: 'app.min.js',
        JASMINE_TEST_CONFIG: __dirname + '/karma-with-jasmine.conf.js',
        MOCHA_TEST_CONFIG: __dirname + '/karma.conf.js',
        PROTRACTOR_CONFIG: 'protractor.conf.js',
        E2E: [
            './tests/e2e/**/*.js',
        ],
    };
const BROWSERS_PREFIX = ['last 3 versions', 'Firefox ESR', 'ie 10'];

const AppName = 'myApp';
const AppNameConfig = AppName + '.config';

gulp.task('connect', () => {
    connect.server({
        root: PATHS.DEST_HTML,
        livereload: true,
        port: 3000
    });
});

gulp.task('sass', () => {
    gulp.src([FILES.BUILD_SASS])
        .pipe(gulpPlumber({
            errorHandler: onError
        }))
        .pipe(gulpSass())
        .pipe(gulpAutoprefixer({
            browsers: BROWSERS_PREFIX,
            remove: false
        }))
        .pipe(gulpConcatCss(FILES.MAIN_CSS))
        .pipe(gulp.dest(PATHS.CSS_FROM_SASS))
        .pipe(gulpMinifyCss())
        .pipe(gulpRename(FILES.MAIN_MIN_CSS))
        .pipe(gulp.dest(PATHS.DEST_CSS))
        .pipe(connect.reload());
});

gulp.task('html', () => {
    gulp.src([FILES.BUILD_HTML])
        .pipe(gulpPlumber({
            errorHandler: onError
        }))
        .pipe(gulpHtmlMin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(PATHS.DEST_HTML))
        .pipe(connect.reload());
});

gulp.task('js-main', () => {
    gulp.src(FILES.BUILD_JS_PARTS)
        .pipe(gulpPlumber({
            errorHandler: onError
        }))
        .pipe(gulpSourcemaps.init())
        .pipe(gulpConcat(FILES.MAIN_MIN_JS))
        .pipe(gulpUglify())
        .pipe(gulpSourcemaps.write())
        .pipe(gulp.dest(PATHS.DEST_JS))
        .pipe(connect.reload());
});

gulp.task('templates', () => {
    return gulp.src([FILES.BUILD_HTML_TEMPLATES, FILES.BUILD_HTML_VIEWS])
        .pipe(gulpAngularTemplateCache({
            standalone: true,
        }))
        .pipe(gulp.dest(PATHS.BUILD_JS));
});

gulp.task('config-json', () => {
    gulp.src(FILES.CONFIG_JSON)
        .pipe(gulpConfig(AppNameConfig))
        .pipe(gulp.dest(PATHS.BUILD_JS));
});

gulp.task('watch', () => {
    gulp.watch(FILES.BUILD_SASS, ['sass']);
    gulp.watch(FILES.BUILD_HTML, ['html']);
    gulp.watch([FILES.BUILD_HTML_TEMPLATES, FILES.BUILD_HTML_VIEWS], ['templates']);
    gulp.watch(FILES.CONFIG_JSON, ['config-json']);
    gulp.watch(FILES.BUILD_JS_PARTS, ['js-main']);
});

gulp.task('unit-test',done => {
    new Server({
        configFile: FILES.MOCHA_TEST_CONFIG,
        singleRun: false
    }, done).start();
});

gulp.task('webdriver_update', webdriver_update);

gulp.task('webdriver_standalone', webdriver_standalone);

gulp.task('protractor', () => {
    gulp.src(FILES.E2E)
        .pipe(protractor({
            configFile: FILES.PROTRACTOR_CONFIG,
        }))
        .on('error', e => console.log(e));
});

gulp.task('e2e-test', ['webdriver_update', 'webdriver_standalone', 'protractor']);

gulp.task('default', ['connect', 'watch']);

function onError(err) {
    console.log('!!!ERROR!!!: ' + err);
}

//just for education to see that e2e tests for chrome and firefox can work without selenium
gulp.task('e2e-without-selenium', ['protractor']);

//just for education example of jasmine tests
gulp.task('unit-test-with-jasmine', done => {
    new Server({
        configFile: FILES.JASMINE_TEST_CONFIG,
        singleRun: false
    }, done).start();
});

//This is only for example for education

/*gulp.task('css-main', () => {
    gulp.src([FILES.BUILD_CSS_VENDOR, FILES.BUILD_CSS_BASE])
        .pipe(gulpPlumber({
            errorHandler: onError
        }))
        .pipe(gulpAutoprefixer({
            browsers: BROWSERS_PREFIX,
            remove: false
        }))
        .pipe(gulpConcatCss(FILES.MAIN_CSS))
        .pipe(gulp.dest(PATHS.BUILD_CSS))
        .pipe(gulpMinifyCss())
        .pipe(gulpRename(FILES.MAIN_MIN_CSS))
        .pipe(gulp.dest(PATHS.DEST_CSS));
});*/